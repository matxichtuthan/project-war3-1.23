#include "Header.h"

DWORD GetDllOffset(const char* dll, int offset)
{
	HMODULE hmod = GetModuleHandle(dll);
	if (!hmod)
		hmod = LoadLibrary(dll);
	if (!hmod) return 0;
	if (offset < 0)
		return (DWORD)GetProcAddress(hmod, (LPCSTR)(-offset));

	return ((DWORD)hmod) + offset;
}

BOOL WriteBytes(LPVOID pAddr, VOID * pData, DWORD dwLen)
{
	DWORD dwOld;

	if (!VirtualProtect(pAddr, dwLen, PAGE_READWRITE, &dwOld))
		return FALSE;

	memcpy(pAddr, pData, dwLen);
	return VirtualProtect(pAddr, dwLen, dwOld, &dwOld);
}

DWORD VirtualProtectEX(DWORD pAddress, DWORD len, DWORD prot)
{
	DWORD oldprot = 0;
	VirtualProtect((void*)pAddress, len, prot, &oldprot);
	return oldprot;
}

void WriteLocalBYTES(DWORD pAddress, void* buf, int len)
{
	DWORD oldprot = VirtualProtectEX(pAddress, len, PAGE_EXECUTE_READWRITE);
	WriteProcessMemory(GetCurrentProcess(), (void*)pAddress, buf, len, 0);
	VirtualProtectEX(pAddress, len, oldprot);
}

void PatchVALUE(DWORD addr, DWORD param, DWORD len)
{
	WriteLocalBYTES(addr, &param, len);
}

void Patch(BYTE bInst, const char* dll, DWORD pAddr, DWORD pFunc, DWORD dwLen, const char* Type)
{
	if (pAddr == 0) return;
	pAddr = GetDllOffset(dll, pAddr);
	BYTE *bCode = new BYTE[dwLen];
	if (bInst)
	{
		::memset(bCode, 0x90, dwLen);
		bCode[0] = bInst;
		if (pFunc)
		{
			if (bInst == 0xE8 || bInst == 0xE9)
			{
				DWORD dwFunc = pFunc - (pAddr + 5);
				*(DWORD*)&bCode[1] = dwFunc;
			}
			else if (bInst == 0x68 || bInst == 0x05 || bInst == 0x5B)
			{
				*(LPDWORD)&bCode[1] = pFunc;
			}
			else if (bInst == 0x83)
			{
				*(WORD*)&bCode[1] = (WORD)pFunc;
			}
			else
			{
				bCode[1] = (BYTE)pFunc;
			}
		}
	}
	else
	{
		if (dwLen == 6)
		{
			::memset(bCode, 0x00, dwLen);
			*(DWORD*)&bCode[0] = pFunc;
		}
		else if (dwLen == 4)
			*(DWORD*)&bCode[0] = pFunc;
		else if (dwLen == 3)
		{
			PatchVALUE(pAddr, pFunc, dwLen);
		}
		else if (dwLen == 2)
			*(WORD*)&bCode[0] = (WORD)pFunc;
		else if (dwLen == 1)
			*(BYTE*)&bCode[0] = (BYTE)pFunc;
		else {
			memcpy(bCode, (void*)pFunc, dwLen);
		}
	}

	if (!WriteBytes((void*)pAddr, bCode, dwLen))
	{
		delete[] bCode;
	}
	delete[] bCode;
}
