#include "Header.h"

uintptr_t address_Storm_SStrCopy = 0;

string V_ConfigAdmin;
char V_Name[MAX_PATH] = { '\0' };
char V_NameOld[MAX_PATH] = { '\0' };

void Config_OnLoad()
{
	char filename[MAX_PATH] = {};
	GetCurrentDirectory(MAX_PATH, filename);
	V_ConfigAdmin.assign(filename);
	V_ConfigAdmin += "\\Project.ini";

	GetPrivateProfileString("Event", "NameOLD", "", V_NameOld, sizeof(V_NameOld), V_ConfigAdmin.c_str());
	GetPrivateProfileString("Event", "Name", "", V_Name, sizeof(V_Name), V_ConfigAdmin.c_str());
}

int __stdcall SStrCopy_Proxy(char* dest, const char* source, size_t size)
{
		int nLen = strlen(V_Name);
		if (nLen)
		{
			string szdest = source;
			if (szdest.length())
			{
				if (szdest == string(V_NameOld))
				{
					source = V_Name;
				}
			}
		}
	return aero::generic_std_call<int>(address_Storm_SStrCopy, dest, source, size);
}

BOOL __stdcall DllMain(HMODULE module, DWORD reason, LPVOID pReserved)
{
	if (reason == DLL_PROCESS_ATTACH)
	{
		Config_OnLoad();
		address_Storm_SStrCopy = GetDllOffset("Storm.dll", -501);
		Detour::Install(&address_Storm_SStrCopy, (uintptr_t)SStrCopy_Proxy);

		Patch(CUSTOM, "Game.dll", 0x7AA2D8 + 1, 0x85, 1, "Name Color");
	}
	else if (reason == DLL_PROCESS_DETACH)
	{
		Detour::Uninstall(&address_Storm_SStrCopy, (uintptr_t)SStrCopy_Proxy);
	}
	return TRUE;
}
