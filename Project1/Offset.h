#pragma once

static const DWORD CALL = 0xE8;
static const DWORD JUMP = 0xE9;
static const DWORD NOP = 0x90;
static const DWORD RET = 0xC3;
static const DWORD XOR = 0x33;
static const DWORD CUSTOM = 0;

DWORD GetDllOffset(const char* dll, int offset);
void Patch(BYTE bInst, const char* dll, DWORD pAddr, DWORD pFunc, DWORD dwLen, const char* Type);