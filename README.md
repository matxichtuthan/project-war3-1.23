- Chỉ đổi tên + thêm tên màu dành cho GTV War3 1.23
- Link đã Build sẵn File: [Release](https://gitlab.com/matxichtuthan/project-war3-1.23/-/tree/main/Release)
- Tải sẵn 2 File Project.ini và Project.mix rồi copy 2 file vào thư mục GPlay\Game\warcraft-3-123.
- Mở file Project.ini có 2 dòng:
    NameOld=Tên hiển thị GTV của bạn ở chỗ thông tin cá nhân (ko phải tên tk).
    Name=tên bạn muốn đổi.
- Mã màu xem ở đây: [Hiveworkshop](https://hiveworkshop.com/threads/warcraft-iii-color-tags-and-linebreaks.31386/)
